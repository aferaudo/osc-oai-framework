#ifndef	_WRAPPER_H_
#define	_WRAPPER_H_


#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>




#include "headers/kpm/ie/kpm_data_ie/kpm_ric_info/kpm_ric_action_def.h"
#include "headers/kpm/ie/kpm_data_ie/kpm_ric_info/kpm_ric_action_def_frm_1.h"
#include "headers/kpm/ie/kpm_data_ie/kpm_ric_info/kpm_ric_action_def_frm_4.h"
#include "headers/kpm/ie/kpm_data_ie/data/matching_cond_frm_4_lst.h"
#include "headers/kpm/ie/kpm_data_ie/data/test_info_lst.h"
#include "headers/kpm/ie/kpm_data_ie/data/meas_info_frm_1_lst.h"

//  E2SM
#include <E2SM-KPM-ActionDefinition.h>
#include <E2SM-KPM-RANfunction-Description.h>
#include <E2SM-KPM-IndicationHeader.h>
#include <E2SM-KPM-IndicationMessage.h>
#include <E2SM-KPM-IndicationMessage.h>

#include <RIC-ReportStyle-Item.h>
#include <MeasurementInfo-Action-Item.h>
#include <asn_application.h>
#include <constr_TYPE.h>


#include "headers/kpm/enc/kpm_enc_asn.h"
#include "headers/kpm/dec/kpm_dec_asn.h"
#include "headers/rc/dec/rc_dec_asn.h"


typedef struct 
{

    long format;
    // measNames
    uint8_t **names;
    size_t names_len;

    // measIds
    long *ids;

}kpm_act_def_cus_t;

typedef struct 
{
    size_t len; 
    kpm_act_def_cus_t *values;

}kpm_act_def_arr_t;



typedef struct 
{
    size_t meas_data_lst_len;
    meas_data_lst_t *meas_data_lst;
    size_t meas_type_len;
    meas_type_t *meas_type;
}meas_data_basic_t;

typedef struct
{
    size_t ue_meas_report_lst_len;
    meas_data_basic_t *meas_data;
}meas_data_basic_mue_t;


__attribute__((visibility("default")))  byte_array_t cp_str_to_ba(const char* str);
__attribute__((visibility("default")))  kpm_act_def_t gen_act_def(const char** act,  uint32_t gran_period_ms, long format);
__attribute__((visibility("default")))  byte_array_t encode_action_def(const char** act,  uint32_t gran_period_ms, long format);
__attribute__((visibility("default")))  byte_array_t encode_ev_trigger(uint32_t ev_trig_period); // temporary to be fixed
__attribute__((visibility("default")))  meas_data_basic_mue_t get_basic_meas_info(size_t len, uint8_t const ind_msg[len]);
__attribute__((visibility("default")))  kpm_act_def_arr_t get_ran_func_def_kpm_oai_wrap(size_t len, uint8_t *buf);
__attribute__((visibility("default"))) void free_byte_array(byte_array_t ba);
__attribute__((visibility("default"))) void free_kpm_ran_func_arr(kpm_act_def_arr_t *actions);
__attribute__((visibility("default"))) void free_meas_basic(meas_data_basic_mue_t *measdata);
__attribute__((visibility("default"))) void get_ran_func_def_rc(const char *hex_values);






#endif /* _WRAPPER_H_ */