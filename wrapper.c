#include "wrapper.h"
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>

#include "headers/kpm/enc/enc_asn/enc_ric_action_def_frm_1.h"
#include "headers/kpm/enc/enc_asn/enc_ric_action_def_frm_2.h"
#include "headers/kpm/enc/enc_asn/enc_ric_action_def_frm_3.h"
#include "headers/kpm/enc/enc_asn/enc_ric_action_def_frm_4.h"
#include "headers/kpm/enc/enc_asn/enc_ric_action_def_frm_5.h"




#include "headers/kpm/dec/dec_asn/dec_ric_ind_hdr_frm_1.h"
#include "headers/kpm/dec/dec_asn/dec_ric_ind_msg_frm_1.h"
#include "headers/kpm/dec/dec_asn/dec_ric_ind_msg_frm_2.h"
#include "headers/kpm/dec/dec_asn/dec_ric_ind_msg_frm_3.h"



byte_array_t hex_to_byte(const char *hex_values)
{
  byte_array_t ba;

  int BUFFER_SIZE=10240;
  // Calculate the length of the hex string
  ba.len = strlen(hex_values);

  // Allocate memory for a char array to store the hex values
  ba.buf = (uint8_t *)malloc(ba.len / 2 + 1); // Each byte is represented by 2 characters, +1 for null terminator

  if (ba.buf == NULL) {
          fprintf(stderr, "Memory allocation failed\n");
  }

  // Convert the hex string to binary data
  for (size_t i = 0; i < ba.len; i += 2) {
          char byte[3] = {hex_values[i], hex_values[i + 1], '\0'};
          ba.buf[i / 2] = (u_int8_t)strtol(byte, NULL, 16);
  }

  
  // Null-terminate the char array
  ba.buf[ba.len / 2] = '\0';

  // Print the result
  printf("Hex values as a string: %s\n", ba.buf);

  return ba;

}

void free_byte_array(byte_array_t ba)
{
  free(ba.buf);
}


void free_kpm_ran_func_value(kpm_act_def_cus_t *value)
{
  if (value == NULL) return;
    
  // Free the names array
  if (value->names != NULL) {
      for (size_t i = 0; i < value->names_len; ++i) {
          free(value->names[i]);
      }
      free(value->names);
  }
  
  // Free the ids array
  if (value->ids != NULL) {
      free(value->ids);
  }
}

void free_meas_data_basic_t(meas_data_basic_t *data)
{
  if (data == NULL) return;
    
  // Free the meas_data_lst array
  if (data->meas_data_lst != NULL) {
      free(data->meas_data_lst);
  }
  
  // Free the meas_type array
  if (data->meas_type != NULL) {
      free(data->meas_type);
  }
}

static
meas_info_format_1_lst_t gen_meas_info_format_1_lst(const char* action)
{
  meas_info_format_1_lst_t dst = {0};

  dst.meas_type.type = NAME_MEAS_TYPE;
  // ETSI TS 128 552
  dst.meas_type.name = cp_str_to_ba(action);

  dst.label_info_lst_len = 1;
  dst.label_info_lst = calloc(1, sizeof(label_info_lst_t));
  assert(dst.label_info_lst != NULL && "Memory exhausted");
  dst.label_info_lst[0].noLabel = calloc(1, sizeof(enum_value_e));
  assert(dst.label_info_lst[0].noLabel != NULL && "Memory exhausted");
  *dst.label_info_lst[0].noLabel = TRUE_ENUM_VALUE;

  return dst;
}

static 
test_info_lst_t filter_predicate(test_cond_type_e type, test_cond_e cond, int value)
{
  test_info_lst_t dst = {0};

  dst.test_cond_type = type;
  // It can only be TRUE_TEST_COND_TYPE so it does not matter the type
  // but ugly ugly...
  dst.S_NSSAI = TRUE_TEST_COND_TYPE;

  dst.test_cond = calloc(1, sizeof(test_cond_e));
  assert(dst.test_cond != NULL && "Memory exhausted");
  *dst.test_cond = cond;

  dst.test_cond_value = calloc(1, sizeof(test_cond_value_t));
  assert(dst.test_cond_value != NULL && "Memory exhausted");
  dst.test_cond_value->type = OCTET_STRING_TEST_COND_VALUE;

  dst.test_cond_value->octet_string_value = calloc(1, sizeof(byte_array_t));
  assert(dst.test_cond_value->octet_string_value != NULL && "Memory exhausted");
  const size_t len_nssai = 1;
  dst.test_cond_value->octet_string_value->len = len_nssai;
  dst.test_cond_value->octet_string_value->buf = calloc(len_nssai, sizeof(uint8_t));
  assert(dst.test_cond_value->octet_string_value->buf != NULL && "Memory exhausted");
  dst.test_cond_value->octet_string_value->buf[0] = value;

  return dst;

} 



static
kpm_act_def_format_1_t gen_act_def_frmt_1(const char** action, uint32_t gran_period_ms)
{
  kpm_act_def_format_1_t dst = {0};

  dst.gran_period_ms = gran_period_ms;

  // [1, 65535]
  size_t count = 0;
  while (action[count] != NULL) {
    count++;
  }
  dst.meas_info_lst_len = count;
  dst.meas_info_lst = calloc(count, sizeof(meas_info_format_1_lst_t));
  assert(dst.meas_info_lst != NULL && "Memory exhausted");

  for(size_t i = 0; i < dst.meas_info_lst_len; i++) {
    dst.meas_info_lst[i] = gen_meas_info_format_1_lst(action[i]);
  }

  return dst;
}

static kpm_act_def_format_3_t 
gen_act_def_frmt_3(const char **action, uint32_t gran_period_ms)
{
  // action so far are only names -- id not supported
  kpm_act_def_format_3_t dst = {0};

  dst.gran_period_ms = gran_period_ms;
  size_t count = 0;
  while (action[count] != NULL) {
    count++;
  }

  dst.meas_info_lst_len = count;
  dst.meas_info_lst = calloc(count, sizeof(meas_info_format_3_lst_t));

  for(size_t i = 0; i < dst.meas_info_lst_len; i++)
  {
    dst.meas_info_lst[i].meas_type.type = NAME_MEAS_TYPE;
    dst.meas_info_lst[i].meas_type.name = cp_str_to_ba(action[i]); 
    
    // static matching condition len (This should be set dynamically in the future)
    // Generating matchin condition list for format 3
    // TODO -- this is too hard coded, it should be changed from the xApp
    dst.meas_info_lst[i].matching_cond_lst_len = 1;
    dst.meas_info_lst[i].matching_cond_lst = calloc(1, sizeof(matching_condition_format_3_lst_t));
    
    // label by default no label -- inspired by kpimon
    dst.meas_info_lst[i].matching_cond_lst[0].cond_type = LABEL_INFO;
    dst.meas_info_lst[i].matching_cond_lst[0].label_info_lst.noLabel = TRUE_ENUM_VALUE;

    // TEST case
    // dst.meas_info_lst[i].matching_cond_lst[0].test_info_lst = filter_predicate();
  }

  return dst;
}

static kpm_act_def_format_4_t 
gen_act_def_frmt_4(const char** action, uint32_t gran_period_ms)
{
  kpm_act_def_format_4_t dst = {0};

  // [1, 32768]
  dst.matching_cond_lst_len = 1;

  dst.matching_cond_lst = calloc(dst.matching_cond_lst_len, sizeof(matching_condition_format_4_lst_t));
  assert(dst.matching_cond_lst != NULL && "Memory exhausted");

  // Filter connected UEs by S-NSSAI criteria
  test_cond_type_e const type = S_NSSAI_TEST_COND_TYPE;
  test_cond_e const condition = EQUAL_TEST_COND;
  int const value = 1;
  dst.matching_cond_lst[0].test_info_lst = filter_predicate(type, condition, value);

  // printf("[xApp]: Filter UEs by S-NSSAI criteria where SST = %lu\n", *dst.matching_cond_lst[0].test_info_lst.test_cond_value->int_value);

  // Action definition Format 1
  dst.action_def_format_1 = gen_act_def_frmt_1(action, gran_period_ms);  // 8.2.1.2.1

  return dst;
}


__attribute__((visibility("default"))) kpm_act_def_arr_t 
get_ran_func_def_kpm_oai_wrap(size_t len, uint8_t *buf)
{
  kpm_act_def_arr_t ret = {0};
  kpm_ran_function_def_t act_def = kpm_dec_func_def_asn(len, buf);
  ret.len = act_def.sz_ric_report_style_list;
  ret.values = (kpm_act_def_cus_t *)calloc(ret.len, sizeof(kpm_act_def_cus_t));
  for (size_t i = 0; i < ret.len; i ++)
  {
    ret.values[i].format = act_def.ric_report_style_list[i].act_def_format_type;
    ret.values[i].names_len = act_def.ric_report_style_list[i].meas_info_for_action_lst_len;
    ret.values[i].names = (uint8_t *)calloc(ret.values[i].names_len, sizeof(uint8_t*));
    ret.values[i].ids = (long *)calloc(ret.values[i].names_len, sizeof(long*));

    for(size_t j=0; j < ret.values[i].names_len; j++)
    {
      ret.values[i].names[j] = calloc(act_def.ric_report_style_list[i].meas_info_for_action_lst[j].name.len, sizeof(uint8_t*));
      memcpy(ret.values[i].names[j],  act_def.ric_report_style_list[i].meas_info_for_action_lst[j].name.buf, act_def.ric_report_style_list[i].meas_info_for_action_lst[j].name.len);

      if(act_def.ric_report_style_list[i].meas_info_for_action_lst[j].id != NULL)
      {
        ret.values[i].ids[j] = calloc(1, sizeof(uint16_t));
        ret.values[i].ids[j] = act_def.ric_report_style_list[i].meas_info_for_action_lst[j].id;
      }
    }
  }

  return ret;
}


void get_ran_func_def_rc(const char *hex_values)
{
  byte_array_t ba = hex_to_byte(hex_values);
}

 __attribute__((visibility("default"))) byte_array_t 
encode_action_def(const char** act, uint32_t gran_period_ms, long format)
{
  kpm_act_def_t action_def = gen_act_def(act, gran_period_ms, format);
  byte_array_t ba = {0};
  if(action_def.type != END_ACTION_DEFINITION)
    ba  =  kpm_enc_action_def_asn(&action_def);
  
  return ba;
}

__attribute__((visibility("default")))  
byte_array_t encode_ev_trigger(uint32_t ev_trig_period)
{
  kpm_event_trigger_def_t event_trigger = {0};
  event_trigger.type = FORMAT_1_RIC_EVENT_TRIGGER;
  event_trigger.kpm_ric_event_trigger_format_1.report_period_ms = ev_trig_period;
  return kpm_enc_event_trigger_asn(&event_trigger);
}


 __attribute__((visibility("default"))) 
kpm_act_def_t gen_act_def(const char** act,  uint32_t gran_period_ms, long format)
{
  kpm_act_def_t dst = {0};
  dst.type = END_ACTION_DEFINITION;
  assert(format != END_ACTION_DEFINITION);
  if(format == FORMAT_1_ACTION_DEFINITION)
  {
    dst.type = FORMAT_1_ACTION_DEFINITION;
    dst.frm_1 = gen_act_def_frmt_1(act, gran_period_ms);
  }
  else if(format == FORMAT_2_ACTION_DEFINITION)
  {
    printf("FORMAT %d NOT SUPPORTED YET\n", FORMAT_2_ACTION_DEFINITION);
  }
  else if(format == FORMAT_3_ACTION_DEFINITION)
  {
    dst.type = FORMAT_3_ACTION_DEFINITION;
    dst.frm_3 = gen_act_def_frmt_3(act, gran_period_ms);
  }
  else if(format == FORMAT_4_ACTION_DEFINITION)
  {
    dst.type = FORMAT_4_ACTION_DEFINITION;
    dst.frm_4 = gen_act_def_frmt_4(act, gran_period_ms);
  }
  else
  {
    printf("FORMAT %ld NOT DEFINED\n", format);
  }
  // dst = gen_action_definition_funcs[format];
  return dst;
}

__attribute__((visibility("default"))) 
byte_array_t cp_str_to_ba(const char* str)
{
  assert(str != NULL);
  size_t const sz = strlen(str);
  byte_array_t dst = {.len = sz }; 
  dst.buf = calloc(sz ,sizeof(uint8_t) );
  assert(dst.buf != NULL);
  memcpy(dst.buf, str, sz);

  return dst;

}


__attribute__((visibility("default")))  
meas_data_basic_mue_t get_basic_meas_info(size_t len, uint8_t const ind_msg[len])
{
  kpm_ind_msg_t msg = kpm_dec_ind_msg_asn(len, ind_msg);
  meas_data_basic_mue_t result = {0};
 
  if(msg.type == FORMAT_3_INDICATION_MESSAGE)
  {
    result.ue_meas_report_lst_len = msg.frm_3.ue_meas_report_lst_len;
    size_t i = 0;
    size_t j;

    result.meas_data = calloc(msg.frm_3.ue_meas_report_lst_len, sizeof(meas_data_basic_t));

    for(i = 0; i < msg.frm_3.ue_meas_report_lst_len; i++)
    {
      // meas data list allocation
      result.meas_data[i].meas_data_lst_len = msg.frm_3.meas_report_per_ue[i].ind_msg_format_1.meas_data_lst_len;
      result.meas_data[i].meas_data_lst = calloc(result.meas_data[i].meas_data_lst_len, sizeof(meas_data_lst_t));

      //meas types allocation
      result.meas_data[i].meas_type_len = msg.frm_3.meas_report_per_ue[i].ind_msg_format_1.meas_info_lst_len;
      result.meas_data[i].meas_type = calloc(result.meas_data[i].meas_type_len , sizeof(meas_type_t));

      // list 1 initialization
      for(j = 0; j < msg.frm_3.meas_report_per_ue[i].ind_msg_format_1.meas_data_lst_len; j ++)
      {
        result.meas_data[i].meas_data_lst[j] = msg.frm_3.meas_report_per_ue[i].ind_msg_format_1.meas_data_lst[j];
      }

      // list 2 initialization
      for(j = 0; j < result.meas_data[i].meas_type_len; j++)
      {
        result.meas_data[i].meas_type[j] =  msg.frm_3.meas_report_per_ue[i].ind_msg_format_1.meas_info_lst[j].meas_type;
      }
    }
  }

  return result;
}



void free_kpm_ran_func_arr(kpm_act_def_arr_t *actions)
{
   if (actions == NULL) return;
    
  // Free each kpm_act_def_cus_t in the array
  if (actions->values != NULL) {
      for (size_t i = 0; i < actions->len; ++i) {
          free_kpm_ran_func_value(&(actions->values[i]));
      }
      free(actions->values);
  }
}

void free_meas_basic(meas_data_basic_mue_t *measdata)
{

  if (measdata == NULL) return;
    
    // Free the meas_data array
    if (measdata->meas_data != NULL) {
        for (size_t i = 0; i < measdata->ue_meas_report_lst_len; ++i) {
            free_meas_data_basic_t(&(measdata->meas_data[i]));
        }
        free(measdata->meas_data);
    }

}