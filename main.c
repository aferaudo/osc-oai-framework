#include<stdio.h>
#include "wrapper.h"
#include <string.h>

static byte_array_t hex_to_byte(const char *hex_values)
{
  byte_array_t ba;

  int BUFFER_SIZE=10240;
  // Calculate the length of the hex string
  ba.len = strlen(hex_values);

  // Allocate memory for a char array to store the hex values
  ba.buf = (uint8_t *)malloc(ba.len / 2 + 1); // Each byte is represented by 2 characters, +1 for null terminator

  if (ba.buf == NULL) {
          fprintf(stderr, "Memory allocation failed\n");
  }

  // Convert the hex string to binary data
  for (size_t i = 0; i < ba.len; i += 2) {
          char byte[3] = {hex_values[i], hex_values[i + 1], '\0'};
          ba.buf[i / 2] = (u_int8_t)strtol(byte, NULL, 16);
  }

  
  // Null-terminate the char array
  ba.buf[ba.len / 2] = '\0';

  // Print the result
  printf("Hex values as a string: %s\n", ba.buf);

  return ba;

}


int main()
{
    // Test encoding decoding action definition
    // const char *act_enb[] = {"CQI", "DRB.PacketSuccessRateUlgNBUu", "DRB.RlcPacketDropRateDl", "DRB.RlcSduTransmittedVolumeDL", "DRB.RlcSduTransmittedVolumeUL", "DRB.UEThpDl", "DRB.UEThpUl", "RSRP", "RSRQ", NULL};
    
    // action definition example generation

    // uint8_t buffer[] = {96,48,79,82,65,78,45,69,50,83,77,45,75,80,77,0,0,24,49,46,51,46,54,46,49,46,52,46,49,46,53,51,49,52,56,46,49,46,51,46,50,46,50,5,0,75,80,77,32,77,111,110,105,116,111,114,0,1,1,7,0,80,101,114,105,111,100,105,99,32,82,101,112,111,114,116,1,1,0,1,4,21,128,67,111,109,109,111,110,32,67,111,110,100,105,116,105,111,110,45,98,97,115,101,100,44,32,85,69,45,108,101,118,101,108,32,77,101,97,115,117,114,101,109,101,110,116,1,4,0,6,2,64,68,82,66,46,80,100,99,112,83,100,117,86,111,108,117,109,101,68,76,2,64,68,82,66,46,80,100,99,112,83,100,117,86,111,108,117,109,101,85,76,2,0,68,82,66,46,82,108,99,83,100,117,68,101,108,97,121,68,108,1,64,68,82,66,46,85,69,84,104,112,68,108,1,64,68,82,66,46,85,69,84,104,112,85,108,1,96,82,82,85,46,80,114,98,84,111,116,68,108,1,96,82,82,85,46,80,114,98,84,111,116,85,108,1,1,1,3};
    // byte_array_t ba;
    // ba.len = sizeof(buffer) / sizeof(buffer[0]);
    // ba.buf = buffer;
    // kpm_act_def_arr_t test = get_ran_func_def_kpm_oai_wrap(ba.len, ba.buf);
    // free_kpm_ran_func_arr(&test);
    // byte_array_t action_def = encode_action_def(act_enb,1000,4);
    // printf("%d\n", action_def.type);

    // byte_array_t ba  = kpm_enc_action_def_asn(&action_def);
   
    // int i;
    // for(i = 0; i < ba.len; i++)
    // {
    //     printf("%d,",ba.buf[i]);
    // }
    // printf("\n");

    // Action definition new method:
    // const char *action_def = "60304F52414E2D4532534D2D4B504D000018312E332E362E312E342E312E35333134382E312E322E322E3205004B504D204D6F6E69746F720001010700506572696F646963205265706F7274010110010109004532204E6F6465204D6561737572656D656E7401010008004043514903604452422E5061636B65745375636365737352617465556C674E42557502C04452422E526C635061636B657444726F7052617465446C03804452422E526C635364755472616E736D6974746564566F6C756D65444C03804452422E526C635364755472616E736D6974746564566F6C756D65554C01404452422E5545546870446C01404452422E5545546870556C0060525352500060525352510101010100010211004532204E6F6465204D6561737572656D656E7420666F7220612073696E676C6520554501020008004043514903604452422E5061636B65745375636365737352617465556C674E42557502C04452422E526C635061636B657444726F7052617465446C03804452422E526C635364755472616E736D6974746564566F6C756D65444C03804452422E526C635364755472616E736D6974746564566F6C756D65554C01404452422E5545546870446C01404452422E5545546870556C006052535250006052535251010101010001031600436F6E646974696F6E2D62617365642C2055452D6C6576656C204532204E6F6465204D6561737572656D656E7401030008004043514903604452422E5061636B65745375636365737352617465556C674E42557502C04452422E526C635061636B657444726F7052617465446C03804452422E526C635364755472616E736D6974746564566F6C756D65444C03804452422E526C635364755472616E736D6974746564566F6C756D65554C01404452422E5545546870446C01404452422E5545546870556C006052535250006052535251010101020001041580436F6D6D6F6E20436F6E646974696F6E2D62617365642C2055452D6C6576656C204D6561737572656D656E7401040008004043514903604452422E5061636B65745375636365737352617465556C674E42557502C04452422E526C635061636B657444726F7052617465446C03804452422E526C635364755472616E736D6974746564566F6C756D65444C03804452422E526C635364755472616E736D6974746564566F6C756D65554C01404452422E5545546870446C01404452422E5545546870556C0060525352500060525352510101010300010511804532204E6F6465204D6561737572656D656E7420666F72206D756C7469706C652055457301050008004043514903604452422E5061636B65745375636365737352617465556C674E42557502C04452422E526C635061636B657444726F7052617465446C03804452422E526C635364755472616E736D6974746564566F6C756D65444C03804452422E526C635364755472616E736D6974746564566F6C756D65554C01404452422E5545546870446C01404452422E5545546870556C00605253525000605253525101010103";
    // byte_array_t ba = hex_to_byte(action_def);
    // kpm_act_def_arr_t ran_func_def = get_ran_func_def_kpm_oai_wrap(ba.len, ba.buf);
//     e2sm_rc_func_def_t test = rc_dec_func_def_asn(ba.len, ba.buf);

//     printf("%s\n", test.name.description.buf);
//     printf("%s\n", test.name.name.buf);
//     printf("%s\n", test.name.oid.buf);

//     if(test.ctrl)
//     {
//         for(size_t i = 0; i < test.ctrl->sz_seq_ctrl_style; i++)
//         {
//                 printf("Control %ld: %s\n", i, test.ctrl->seq_ctrl_style[i].name.buf);
//                 printf("Header: %d", test.ctrl->seq_ctrl_style[i].
//         }
//     }

    // for(size_t i = 0; i < ran_func_def.len; i++)
    // {
    //     if(ran_func_def.values[i].format == FORMAT_1_ACTION_DEFINITION)
    //     {
    //         printf("there is a format 1\n");
    //     }
    //     else if(ran_func_def.values[i].format == FORMAT_2_ACTION_DEFINITION)
    //     {
    //         printf("there is a format 2\n");
    //     }
    //     else if(ran_func_def.values[i].format == FORMAT_3_ACTION_DEFINITION)
    //     {
    //         printf("there is a format 3\n");
    //     }
        
    // }

    // byte_array_t ba2 =  encode_action_def(act_enb, 1000, FORMAT_3_ACTION_DEFINITION);
    // for(size_t i = 0; i < ba2.len; i ++)
    // {
    //    printf("%d,",ba2.buf[i]);
    // }
    // printf("\n");
    // int i, j;
    // for(i = 0; i < ran_func_def.len; i++)
    // {
    //     printf("Format: %d\n", ran_func_def.values[i].format);
    //     for(j = 0; j < ran_func_def.values[i].names_len; j++)
    //     {
    //         printf("IDs: ");
    //         if(ran_func_def.values[i].ids != NULL)
    //             printf("%d, ", ran_func_def.values[i].ids[j]);
    //         printf("\tNames: ");
    //          if(ran_func_def.values[i].names != NULL)
    //             printf("%s, ", ran_func_def.values[i].names[j]);
    //         printf("\n");
    //     }
    // }

    // kpm_act_def_t action_def_dec = kpm_dec_action_def_asn(ba.len, ba.buf);
    // printf("%d\n", action_def_dec.type);
    // uint8_t buffer[] = {8, 91, 60, 98, 102, 0, 0, 9, 110, 103, 114, 97, 110, 95, 103, 78, 66};
    // uint8_t buffer2[] = {64,0,128,160,0,0,0,0,128,12,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,64,0,0,0,6,0,0,0,0,32,0,32,0,0,0,0,0,0,5,1,32,68,82,66,46,80,100,99,112,83,100,117,86,111,108,117,109,101,68,76,1,32,0,0,1,32,68,82,66,46,80,100,99,112,83,100,117,86,111,108,117,109,101,85,76,1,32,0,0,0,160,68,82,66,46,85,69,84,104,112,68,108,1,32,0,0,0,160,68,82,66,46,85,69,84,104,112,85,108,1,32,0,0,0,176,82,82,85,46,80,114,98,84,111,116,68,108,1,32,0,0,0,176,82,82,85,46,80,114,98,84,111,116,68,108,1,32,0,0};
    // uint8_t ind_msg [] = {64, 0, 128, 160, 0, 0, 0, 0, 128, 11, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 64, 0, 0, 0, 6, 0, 0, 0, 0, 32, 0, 32, 0, 0, 0, 0, 0, 0, 5, 1, 32, 68, 82, 66, 46, 80, 100, 99, 112, 83, 100, 117, 86, 111, 108, 117, 109, 101, 68, 76, 1, 32, 0, 0, 1, 32, 68, 82, 66, 46, 80, 100, 99, 112, 83, 100, 117, 86, 111, 108, 117, 109, 101, 85, 76, 1, 32, 0, 0, 0, 160, 68, 82, 66, 46, 85, 69, 84, 104, 112, 68, 108, 1, 32, 0, 0, 0, 160, 68, 82, 66, 46, 85, 69, 84, 104, 112, 85, 108, 1, 32, 0, 0, 0, 176, 82, 82, 85, 46, 80, 114, 98, 84, 111, 116, 68, 108, 1, 32, 0, 0, 0, 176, 82, 82, 85, 46, 80, 114, 98, 84, 111, 116, 68, 108, 1, 32, 0, 0};
    // Create and initialize the byte_array_t structure
    // u_int8_t ind_msg[] = {12, 0, 0, 64, 1, 0, 1, 0, 0, 0, 32, 0, 0, 1, 24, 0, 0, 55, 52, 55, 76, 64, 49, 48, 48, 0, 0};
    // byte_array_t ba;
    // ba.len = sizeof(ind_msg) / sizeof(ind_msg[0]);
    // ba.buf = ind_msg;

    // size_t i;
    // kpm_ind_msg_t msg = kpm_dec_ind_msg_asn(ba.len, ba.buf);

    // printf("Id: %d\n", msg.frm_1.meas_info_lst->meas_type.id);
    // printf("dim name: %ld\n", msg.frm_1.meas_info_lst->meas_type.name.len);
    // printf("name: %d\n",  msg.frm_1.meas_info_lst->meas_type.name.buf[0]);

    // meas_data_basic_mue_t test = get_meas_data_from_encoded_ind_msg(ba.len, ba.buf);
    // printf("%ld\n", test.meas_data[0].meas_data_lst_len);
    // get_basic_meas_info(ba.len, ba.buf);
    // size_t *len;
    // get_meas_data_from_encoded_ind_msg(ba.len, ba.buf, len);
    // meas_data_cplx_wrapp test = get_basic_meas_info_wrapp(ba.len, ba.buf);
    // for(i = 0; i < test.ue_meas_report_lst_len; i++)
    // {
    //     printf("%d\n", test.meas_data[i].meas_type.type);
    // }
    // test();

    // byte_array_t ba2;
    // ba2.len = sizeof(buffer2) / sizeof(buffer2[0]);
    // ba2.buf = buffer2;


    
    // kpm_ind_hdr_t t = kpm_dec_ind_hdr_asn(ba.len, ba.buf);
    // printf("%d\n", t.type);
    // kpm_ind_msg_t t1 = kpm_dec_ind_msg_asn(ba2.len, ba2.buf);
    // printf("%d\n", t1.type);


    const char *action_def_rc = "6805804F52414E2D4532534D2D5243000018312E332E362E312E342E312E35333134382E312E312E322E33050052414E20436F6E74726F6C000001040A00554520496E666F726D6174696F6E204368616E67650103008001040680554520496E666F726D6174696F6E0103010001000101000000C90400525243205374617465008001010980526164696F2042656172657220436F6E74726F6C00004000010E80516F5320666C6F77206D617070696E6720636F6E66696775726174696F6E000100000280445242204944800113004C697374206F6620516F5320466C6F777320746F206265206D6F64696669656420696E20445242013900000100030900516F5320466C6F77204964656E74696669657200040D00516F5320466C6F77204D617070696E6720496E6469636174696F6E010001000100";
    byte_array_t ba = hex_to_byte(action_def_rc);
    e2sm_rc_func_def_t test = rc_dec_func_def_asn(ba.len, ba.buf);
    

    printf("%s\n", test.name.description.buf);
    int i = 0;
    if(test.ctrl != NULL)
    {
      printf("[CONTROL]\n");
      for(i = 0; i < test.ctrl->sz_seq_ctrl_style; i++)
      {
        printf("Name: %s\n", test.ctrl->seq_ctrl_style[i].name.buf);
        printf("Style: %d\n", test.ctrl->seq_ctrl_style[i].style_type);
      }
    }

    if(test.ev_trig != NULL)
    {
      printf("[EVENT TRIGGER]\n");
      for(i = 0; i < test.ev_trig->sz_seq_ev_trg_style; i++)
      {
        printf("Name: %s\n", test.ev_trig->seq_ev_trg_style[i].name.buf);
        printf("Style: %d\n", test.ev_trig->seq_ev_trg_style[i].style);
      }
    }
    
    if(test.report != NULL)
    {
      printf("[REPORT]\n");

      for(i = 0; i < test.report->sz_seq_report_sty; i++)
      {
        printf("Name: %s\n", test.report->seq_report_sty[i].name.buf);
        printf("Style: %d\n", test.report->seq_report_sty[i].report_type);
      }
    }

    if(test.policy != NULL)
    {
      printf("policy not null\n");
    }
    if(test.insert != NULL)
    {
      printf("insert not null\n");
    }
    // free_e2sm_rc_action_def(&test);

}