# Compiler and flags
CC = gcc
CFLAGS = -Wall -Iheaders $(shell find headers -type d -exec echo -I{} \;) -fPIC
LDFLAGS = -L. -lkpm_sm -lrc_sm -Wl,-rpath=$(shell pwd)

# Source files
SRC = wrapper.c
OBJ = $(SRC:.c=.o)

# Shared library names
SHARED_LIB = libsm_framework.so

# Default rule to build the shared library
all: $(SHARED_LIB)

# Rule to compile source files into object files
%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

# Rule to create the shared library from wrapper.o
$(SHARED_LIB): $(OBJ)
	$(CC) -shared -o $@ $^ $(LDFLAGS)

# Clean rule to remove generated files
clean:
	rm -f $(OBJ) $(SHARED_LIB)

.PHONY: all clean